import React from 'react'
import ReactDOM from 'react-dom'
import App from './containers/App'
import reportWebVitals from './reportWebVitals'
import bridge from '@vkontakte/vk-bridge'
import { AdaptivityProvider, ConfigProvider } from '@vkontakte/vkui'

bridge.send('VKWebAppInit')

ReactDOM.render(
  <React.StrictMode>
    <ConfigProvider>
      <AdaptivityProvider>
        <App />
      </AdaptivityProvider>
    </ConfigProvider>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
