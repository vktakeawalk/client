import client from './client'

interface DownloadFileArgs {
  url: string
}

const downloadFile = async (args: DownloadFileArgs) => {
  const response = await client.sharingGetSharedLinkFile(args)
  return (response as any).fileBlob
}

export default downloadFile
