import { Dropbox } from 'dropbox'

const accessToken =
  process.env.REACT_APP_DROPBOX_ACCESSTOKEN ?? 'REACT_APP_DROPBOX_ACCESSTOKEN'

const client = new Dropbox({ accessToken })

export default client
