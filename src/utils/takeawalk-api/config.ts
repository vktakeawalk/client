class TakeAWalkApiConfig {
  uri: string
  constructor(uri: string) {
    this.uri = uri
  }
}

const config = new TakeAWalkApiConfig(
  process.env.TAKEAWALK_API_URI ?? 'localhost:8080'
)

export default config
