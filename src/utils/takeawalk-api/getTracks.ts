import axios from 'axios'
import config from './config'

const getTracks = async () => {
  await axios.get(`${config.uri}/tracks`)
}

export default getTracks
