import { useEffect, useRef, useState } from 'react'
import VectorLayer from 'ol/layer/Vector'
import VectorSource from 'ol/source/Vector'
import Map from 'ol/Map'
import TileLayer from 'ol/layer/Tile'
import { XYZ } from 'ol/source'
import { Feature, MapBrowserEvent, View } from 'ol'
import { Geometry } from 'ol/geom'
import { transform } from 'ol/proj'
import { Coordinate } from 'ol/coordinate'
import './MapWrapper.css'
import { geolocated } from 'react-geolocated'

interface MapWrapperProps {
  features?: Feature[]
  coords?: GeolocationCoordinates
}

const MapWrapper = (props: MapWrapperProps) => {
  const { coords, features } = props

  const [map, setMap] = useState<Map>()
  const [featuresLayer, setFeaturesLayer] =
    useState<VectorLayer<VectorSource<Geometry>>>()
  const [selectedCoord, setSelectedCoord] = useState<Coordinate>()

  const mapElement = useRef<HTMLElement>()

  const mapRef = useRef<Map>()
  mapRef.current = map

  useEffect(() => {
    // create and add vector source layer
    const initalFeaturesLayer = new VectorLayer({
      source: new VectorSource(),
    })

    // create map
    const initialMap = new Map({
      target: mapElement.current,
      layers: [
        new TileLayer({
          source: new XYZ({
            url: 'http://mt0.google.com/vt/lyrs=p&hl=en&x={x}&y={y}&z={z}',
          }),
        }),

        initalFeaturesLayer,
      ],
      view: new View({
        projection: 'EPSG:3857',
        center: [0, 0],
        zoom: 2,
      }),
      controls: [],
    })

    initialMap.on('click', handleMapClick)

    // save map and vector layer references to state
    setMap(initialMap)
    setFeaturesLayer(initalFeaturesLayer)
  }, [])

  useEffect(() => {
    if (features?.length) {
      // set features to map
      featuresLayer?.setSource(
        new VectorSource({
          features, // make sure features is an array
        })
      )

      // fit map to feature extent (with 100px of padding)
      // @ts-ignore
      map.getView().fit(featuresLayer.getSource().getExtent(), {
        padding: [100, 100, 100, 100],
      })
    }
  }, [features, featuresLayer, map])

  useEffect(() => {
    mapRef.current
      ?.getView()
      .setCenter(
        transform(
          [coords?.longitude ?? 0, coords?.latitude ?? 0],
          'EPSG:4326',
          'EPSG:3857'
        )
      )
    mapRef.current?.getView().setZoom(15)
  }, [coords])

  // map click handler
  const handleMapClick: (event: MapBrowserEvent<any>) => unknown = (event) => {
    // @ts-ignore
    const clickedCoord = mapRef.current.getCoordinateFromPixel(event.pixel)

    // transform coord to EPSG 4326 standard Lat Long
    const transormedCoord = transform(clickedCoord, 'EPSG:3857', 'EPSG:4326')

    // set React state
    setSelectedCoord(transormedCoord)

    console.log(transormedCoord)
  }

  // @ts-ignore
  return <div ref={mapElement} className="map-container" />
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(MapWrapper)
