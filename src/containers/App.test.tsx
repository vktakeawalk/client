import React from 'react'
import { render, screen } from '@testing-library/react'
import App from './App'

test('renders "Hello, take a walk!"', () => {
  render(<App />)
  const textElem = screen.getByText(/Hello, take a walk!/i)
  expect(textElem).toBeInTheDocument()
})
