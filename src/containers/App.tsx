import React from 'react'
import {
  AppRoot,
  SplitCol,
  SplitLayout,
  ViewWidth,
  withAdaptivity,
} from '@vkontakte/vkui'
import MapWrapper from '../components/atoms/MapWrapper'

interface AppProps {
  viewWidth: ViewWidth
}

const App = (props: AppProps) => {
  const { viewWidth } = props

  const isDesktop = viewWidth ?? viewWidth >= ViewWidth.TABLET

  return (
    <AppRoot>
      <SplitLayout>
        {isDesktop && <SplitCol />}
        {!isDesktop && <SplitCol />}
        <SplitCol>
          <MapWrapper />
        </SplitCol>
      </SplitLayout>
    </AppRoot>
  )
}

export default withAdaptivity(App, { viewWidth: true })
